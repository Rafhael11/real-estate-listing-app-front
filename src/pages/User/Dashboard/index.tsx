import React from 'react'

import NewPropertyForm from './forms/newPropertyForm'

const Admin = () => {

  return (
    <div>
      <NewPropertyForm />
    </div>
  )
}

export default Admin