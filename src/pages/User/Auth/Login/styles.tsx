import { makeStyles } from '@mui/styles'

const useStyles: any = makeStyles(() => ({
  loginContainer: {
    margin: '0 auto',
    width: '300px'
  },
}))

export default useStyles

