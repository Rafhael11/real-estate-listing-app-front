export const palette = {
  primary: {
    main: '#1B263B'
  },
  secondary: {
    main: '#415A77'
  },
  text: {
    primary: '#0D1B2A',
    secondary: '#A1BDCF',
  },
}
